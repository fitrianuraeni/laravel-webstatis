<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cast;

use App\Models\Peran;

use App\Models\Film;

class CastController extends Controller
{
    //
    public function create()
    {
    	return view('cast.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required'
    	]);
 
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
            'bio' => $request->bio
    	]);
 
    	return redirect('/cast');
    }

    
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    
    public function show($id)
    {
        $cast = Cast::find($id);
        $peran = Peran::where('cast_id', $id)->get();
        return view('cast.show', compact('cast','peran'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $cast = Cast::find($id);
        $peran = Peran::where('cast_id', $id)->delete();
        $cast->delete();
        return redirect('/cast');
    }

    public function tambahPeran($id)
    {
        $cast = Cast::find($id);
        $film = Film::all();
        return view('cast.peran', compact('cast','film'));;
    }

    public function simpanPeran($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'film_id' => 'required|numeric'
        ]);

        Peran::create([
    		'nama' => $request->nama,
    		'film_id' => $request->film_id,
            'cast_id' => $id
    	]);

        $cast = Cast::find($id);
        $peran = Peran::where('cast_id', $id)->get();
        return view('cast.show', compact('cast','peran'));;
    }
}
