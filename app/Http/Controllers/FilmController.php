<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

use App\Models\Film;

use App\Models\Genre;

use App\Models\Kritik;

use App\Models\Peran;




class FilmController extends Controller
{
    //
    public function create()
    {
        $genre = Genre::all();
    	return view('film.create',compact('genre') );
    }
 
    public function store(Request $request)
    {
    	//dd($request);
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre' => 'required'
    	]);
 
        Film::create([
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'poster' => $request->poster,
            'genre_id' => $request->genre,
    	]);
 
    	return redirect('/film');
    }

    
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', compact('film','genre'));
    }

    
    public function show($id)
    {
        $film = Film::find($id);
        $kritik = Kritik::where('film_id', $id) -> get();
        $peran = Peran::where('film_id', $id) -> get();

        return view('film.show', compact('film','kritik','peran'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
    		'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre' => 'required'
        ]);

        $film = Film::find($id);
        $film->judul = $request->judul; 
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $request->poster;
        $film->genre_id = $request->genre;
        $film->update();
        return redirect('/film');
    }

    public function destroy($id)
    {
        $film = Film::find($id);
        $kritik = Kritik::where('film_id', $id) -> delete();
        $peran = Peran::where('film_id', $id) -> delete();
        $film->delete();
        return redirect('/film');
    }

    public function simpanKritik($id, Request $request)
    {
        $this->validate($request,[
    		'content' => 'required',
    		'point' => 'required|numeric|min:1|max:5',
    	]);
 
        Kritik::create([
    		'nama' => Auth::user()->name,
    		'user_id' => Auth::user()->id,
            'film_id' => $id,
            'content' => $request->content,
            'point' => $request->point,
    	]);
        
    	return redirect('/film' );
    }
}
