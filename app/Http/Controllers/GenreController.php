<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Genre;

use App\Models\Film;

use App\Models\Kritik;

use App\Models\Peran;


class GenreController extends Controller
{
    //
    public function create()
    {
    	return view('genre.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    	]);
 
        Genre::create([
    		'nama' => $request->nama,
    	]);
 
    	return redirect('/genre');
    }

    
    public function index()
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('genre.edit', compact('genre'));
    }

    
    public function show($id)
    {
        $genre = Genre::find($id);
        $film = Film::where('genre_id', $id)->get();
        return view('genre.show', compact('genre', 'film'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = Genre::find($id);
        $genre->nama = $request->nama;
        $genre->update();
        return redirect('/genre');
    }

    public function destroy($id)
    {
        $genre = Genre::find($id);
        $film = Film::where('genre_id',$id)->get();
        foreach ($film as $key=>$value){
            $film = Film::find($value->id);
            $kritik = Kritik::where('film_id', $value->id) -> delete();
            $peran = Peran::where('film_id', $value->id) -> delete();
            $film->delete();
        }
        $genre->delete();
        return redirect('/genre');
    }
}
