<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Game;

class GameController extends Controller
{
    //
    public function create()
    {
    	return view('game.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'gameplay' => 'required',
            'developer' => 'required',
            'year'=> 'required'
    	]);
 
        Game::create([
            'name' => $request->name,
    		'gameplay' => $request->gameplay,
            'developer' => $request->developer,
            'year'=> $request->year
    	]);
 
    	return redirect('/game');
    }

    
    public function index()
    {
        $game = Game::all();
        return view('game.index', compact('game'));
    }

    public function edit($id)
    {
        $game = Game::find($id);
        return view('game.edit', compact('game'));
    }

    
    public function show($id)
    {
        $game = Game::find($id);
        return view('game.show', compact('game'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
    		'gameplay' => 'required',
            'developer' => 'required',
            'year'=> 'required'
        ]);

        $game = Game::find($id);
        $game->name = $request->name;
        $game->gameplay = $request->gameplay;
        $game->developer = $request->developer;
        $game->year = $request->year;
        $game->update();
        return redirect('/game');
    }

    public function destroy($id)
    {
        $game = Game::find($id);
        $game->delete();
        return redirect('/game');
    }
}
