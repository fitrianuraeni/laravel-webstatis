<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    protected $table = "film";
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"];
    
    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function kritik()
    {
        return $this->hasMany(Kritik::class);
    }

    public function peran()
    {
        return $this->hasMany(Peran::class);
    }
}