@extends('layout.master')

@section('title')
Formulir Genre
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Formulir Genre Baru</h5>
        <div class="card-body">
            <h5 class="card-title"></h5>
            <form action="/genre" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Tulis nama genre disini!">
                </div>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection