@extends('layout.master')

@section('title')
Tampilkan Detail Genre
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">List Film {{$genre->nama}}</h5>
        <div class="card-body">
        <ol>
        @forelse ($film as $key=>$value)
            <li>
                <p>{{$value->judul}}</p>
            </li>
        @empty
                <p>Tidak ada data Film</p>
        @endforelse     
        </ol>
        </div>
        <a href="/genre" class="btn btn-primary mb-3">Kembali</a>
    </div>
@endsection