@extends('layout.master')

@section('title')
Tampilkan Detail Film
@endsection

@section('content')
<div class="media">
  <img src="{{$film->poster}}" class="mr-3" alt="...">
  <div class="media-body">
    <h2 class="mt-0">{{$film->judul}}</h2>
    <ol>
            <li>
                <p>Judul : {{$film->judul}}</p>
            </li>
            <li>
                <p>Ringkasan : {{$film->ringkasan}}</p>
            </li>
            <li>
                <p>Tahun : {{$film->tahun}}</p>
            </li>
            <li>
                <p>Genre : {{$film->genre->nama}}</p>
            </li>
            <li>
                <p>Cast :</p>
                <ul>
                    @forelse ($peran as $key=>$value)
                    <li>
                        <p>{{$value->cast->nama}} sebagai {{$value->nama}}</p>
                    </li>
                    @empty
                        <p>Belum ada cast</p>
                    @endforelse
                </ul>
            </li>
        </ol>
  </div>
</div>

@auth
<div class="card">
    <form action="/kritik/{{ $film->id }}" method="POST">
    @csrf
    <div class="form-group">
        <textarea class="form-control" id="content" rows="3" name="content" placeholder="Tulis review film disini"></textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    <div class="form-group">
        <input type="number" class="form-control" id="point" name="point" placeholder="Nilai film dengan angka 1-5"></input>
    </div>
    @error('point')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    <div class="form-group">
        <button class="btn btn-primary" type="submit">Tambah Review</button>
    </div>
    </form>
</div>
@endauth

    <div class="card">
        <div class="card-header">
            <h5>Review<h5>
        </div>
        @forelse ($kritik as $key=>$value)
        <div class="card-body">
            <blockquote class="blockquote mb-0">
            @for ($x = 0; $x < $value->point; $x++)
                <i class="fas fa-star" style="color: #facc29;"></i>
            @endfor
            <p>{{$value->content}}</p>
            <footer class="blockquote-footer"><cite title="Source Title">{{$value->nama}}</cite></footer>
            </blockquote>
        </div>
        @empty
        <div class="card-body">
            <p>Belum ada review</p>
        </div>
        @endforelse
    </div>

        
        </div>
        </div>
    </div>
@endsection