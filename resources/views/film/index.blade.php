@extends('layout.master')

@section('title')
Daftar Film
@endsection

@section('content')

    <a href="/film/create" class="btn btn-primary mb-3">Tambah</a>
    <div class="row">
        @forelse ($film as $key=>$value)
        <div class="col-sm">
        <div class="card" style="width: 18rem;">
            <img src="{{$value->poster}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3>{{$value->judul}}</h3>
                <p class="card-text">{{$value->ringkasan}}</p>
                <p class="card-text">Tahun : {{$value->tahun}}</p>
                <p class="card-text">Genre : {{$value->genre->nama}}</p>
                <form action="/film/{{$value->id}}" method="POST">
                    @csrf
                    <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </div>
        </div>
</div>
        @empty
                <p>Belum ada film</p>
        @endforelse              
</div>
@endsection