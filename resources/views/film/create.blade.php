@extends('layout.master')

@section('title')
Formulir Film
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Formulir Film Baru</h5>
        <div class="card-body">
            <h5 class="card-title"></h5>
            <form action="/film" method="POST">
                @csrf
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Tulis judul film disini!">
                </div>
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="ringkasan">Ringkasan</label>
                    <textarea rows="3" class="form-control" id="ringkasan" name="ringkasan" placeholder="Tulis ringkasan film disini!"></textarea>
                </div>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Tulis tahun film disini"></input>
                </div>
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="poster">Poster</label>
                    <input type="text" class="form-control" id="poster" name="poster" placeholder="Tulis link poster film disini"></input>
                </div>
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="genre">Genre</label>
                    <select class="form-control" id="genre" name="genre">
                        <option selected>Genre</option>
                        @foreach ($genre as $key=>$value)
                        <option value="{{$value->id}}">{{$value->nama}}</option>
                        @endforeach
                    </select>
                </div>
                @error('genre')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection