@extends('layout.master')

@section('title')
Formulir Edit Film
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Edit Film {{$film->judul}}</h5>
        <div class="card-body">
            <form action="/film/{{$film->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{$film->judul}}">
                </div>
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="ringkasan">Ringkasan</label>
                    <textarea rows="3" class="form-control" id="ringkasan" name="ringkasan">{{$film->ringkasan}}</textarea>
                </div>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="tahun">Tahun</label>
                    <input type="number" class="form-control" id="tahun" name="tahun" value="{{$film->tahun}}"></input>
                </div>
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="poster">Poster</label>
                    <input type="text" class="form-control" id="poster" name="poster" value="{{$film->poster}}"></input>
                </div>
                @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="genre">Genre</label>
                    <select class="form-control" id="genre" name="genre">
                        <option selected value="{{$film->genre_id}}">{{$film->genre->nama}}</option>
                        @foreach ($genre as $key=>$value)
                        <option value="{{$value->id}}">{{$value->nama}}</option>
                        @endforeach
                    </select>
                </div>
                @error('genre')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection