@extends('layout.master')

@section('title')
Halaman Form
@endsection

@section('content')

		<h3>Buat Account Baru</h3>
		<h5>Sign Up Form</h5>
		<form action="/welcome" method="POST">
			@csrf
			<label>First Name :</label><br><br>
			<input type="text" name="firstname"><br><br>
			<label>Last Name :</label><br><br>
			<input type="text" name="lastname"><br><br>
			<label>Gender</label><br><br>
			<input type="radio">Male<br>
			<input type="radio">Female<br><br>
			<label>Nationality</label><br><br>
			<select name="nationality">
				<option value="Indonesia">Indonesia</option>
				<option value="Inggris">Inggris</option>
				<option value="Amerika">Amerika</option>
			</select><br><br>
			<label>Language Spooken</label><br><br>
			<input type="checkbox" name="language">Bahasa Indonesia<br>
			<input type="checkbox" name="language">English<br>
			<input type="checkbox" name="language">Other<br><br>
			<label>Bio</label><br><br>
			<textarea name="bio" rows="10" cols="30"></textarea><br><br>
			<button type="submit" value="Sign Up">Sign Up</button>
		</form>
@endsection