@extends('layout.master')

@section('title')
Halaman Home
@endsection


@section('content')
	@auth
		<h3>Selamat Datang {{ Auth::user()->name }}!</h3>
		<p>Terima kasih telah bergabung di Website Kami. Lihat atau berikan review film disini!</p>
	@endauth
	@guest
		<h3>Selamat Datang!</h3>
		<p>Terima kasih telah mengunjungi Website Kami. Lihat atau berikan review film disini!</p>
	@endguest

@endsection

