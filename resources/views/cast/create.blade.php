@extends('layout.master')

@section('title')
Formulir Cast
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Formulir Cast Baru</h5>
        <div class="card-body">
            <h5 class="card-title"></h5>
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Tulis nama anda disini!">
                </div>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="nama">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Tulis umur anda disini!">
                </div>
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea class="form-control" id="bio" rows="3" name="bio"></textarea>
                </div>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection