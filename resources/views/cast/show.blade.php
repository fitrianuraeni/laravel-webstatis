@extends('layout.master')

@section('title')
Tampilkan Detail Cast
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Show Cast {{$cast->nama}}</h5>
        <div class="card-body">
        <ol>
            <li>
                <p>Nama : {{$cast->nama}}</p>
            </li>
            <li>
                <p>Umur : {{$cast->umur}}</p>
            </li>
            <li>
                <p>Bio : {{$cast->bio}}</p>
            </li>
            @forelse ($peran as $key=>$value)
            <li>
                <p>Peran : Sebagai {{$value->nama}} di film {{$value->film->judul}}</p>
            </li>
            @empty
            <li>
                <p>Peran : Belum ada peran</p>
            </li>
            @endforelse
        </ol>
        </div>
        <a href="/cast" class="btn btn-primary mb-3">Kembali</a>
    </div>
@endsection