@extends('layout.master')

@section('title')
Tambah Peran
@endsection

@section('content')
    <div class="card">
        <h5 class="card-header">Tambah Peran {{$cast->nama}} </h5>
        <div class="card-body">
            <form action="/peran/{{$cast->id}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Tulis nama peran disini!">
                </div>
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <label for="film_id">Judul Film</label>
                    <select class="form-control" id="film_id" name="film_id">
                        <option selected>Judul Film</option>
                        @foreach ($film as $key=>$value)
                        <option value="{{$value->id}}">{{$value->judul}}</option>
                        @endforeach
                    </select>
                </div>
                @error('film_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection